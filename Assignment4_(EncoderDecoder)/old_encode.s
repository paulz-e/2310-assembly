input_str  .req r0
output_str .req r1
key_str    .req r2
switch     .req r3
i          .req r4
j			  .req r5

		.file "encode.s"
		.section .text
		.global encode
		.type encode, %function

encode:
	push {r4-r7, lr}
	mov i, #0
	mov j, #0

id_switch:
	cmp switch, #1
	beq decode_loop

encode_loop:
	ldrb r6, [input_str, i]    // load one byte of input string to r7
	ldrb r7, [key_str, j]      // load one byte of key string to r8

	cmp r6, #0x00
	beq done
	
	cmp r6, #0x20
	beq enc_space
	cmp r7, #0x20
	beq enc_space

	cmp r7, #0x00
	bne continue_enc
	
	mov j, #0
	ldrb r7, [key_str, j]

continue_enc:
	sub r6, r6, #0x60  // subtract 'a'
	sub r7, r7, #0x60

	add r6, r6, r7     // add them together
	cmp r6, #26        // compare to 26
	ble encode_done    // if greater than 26, subtract 26

	sub r6, r6, #26

encode_done:
	add r6, r6, #0x60
	strb r6, [output_str, i]

enc_space:
	strb r6, [output_str, i]

	add i, i, #1
	add j, j, #1
	b encode_loop

decode_loop:
	ldrb r6, [input_str, i]
	ldrb r7, [key_str, j]

	cmp r6,  #0x00
	beq done

	cmp r6, #0x20
	beq dec_space
	cmp r7, #0x20
	beq dec_space

	cmp r7, #0x00
	bne continue_dec

	mov j, #0
	ldrb r7, [key_str, j]

continue_dec:
	sub r6, r6, #0x60
	sub r7, r7, #0x60

	sub r6, r6, r7
	cmp r6, #0
	ble decode_done

	add r6, r6, #26

decode_done:
	add r6, r6, #0x60
	strb r6, [output_str, i]

dec_space:
	strb r6, [output_str, i]

	add i, i, #1
	add j, j, #1
	b decode_loop

done:
	strb r6, [output_str, i]

	.unreq input_str
	.unreq output_str
	.unreq key_str
	.unreq switch
	.unreq i
	.unreq j

	pop {r4-r7, pc}
